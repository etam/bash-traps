# setup traps
declare -A traps
execute_traps() {
    local t
    for t in "${traps[@]}"; do
        eval "$t"
    done
}
trap 'execute_traps' EXIT

# kill jobs on exit
traps["kill jobs"]="jobs -r -p | xargs -r kill"

# do stuff

# now we don't want to wait until exit
eval "${traps["kill jobs"]}"
unset 'traps["kill jobs"]'
